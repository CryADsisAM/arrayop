# pylint: disable=line-too-long
# pylint: disable=invalid-name

"""
Some docstring here
"""
from typing import Tuple
from typing import List
from typing import Union
import array
from math import log,exp

class BaseArray:
    """
    Razred BaseArray, za učinkovito hranjenje in dostopanje do večdimenzionalnih podatkov. Vsi podatki so istega
    številskega tipa: int ali float.

    Pri ustvarjanju podamo obliko-dimenzije tabele. Podamo lahko tudi tip hranjenih podatkov (privzeto je int) in
    podatke za inicializacijo (privzeto vse 0).

    Primeri ustvarjanja:
    a = BaseArray((10,)) # tabela 10 vrednosti, tipa int, vse 0
    a = BaseArray((3, 3), dtype=float) # tabela 9 vrednost v 3x3 matriki, tipa float, vse 0.0
    a = BaseArray((2, 3), dtype=float, dtype=float, data=(1., 2., 1., 2., 1., 2.)) # tabela 6 vrednost v 2x3 matriki,
                                                                               #tipa float, vrednosti 1. in 2.
    a = BaseArray((2, 3, 4, 5), data=tuple(range(120))) # tabela dimenzij 2x3x4x5, tipa int, vrednosti od 0 do 120


    Do podatkov dostopamo z operatorjem []. Pri tem moramo paziti, da uporabimo indekse, ki so znotraj oblike tabele.
    Začnemo z indeksom 0, zadnji indeks pa je dolžina dimenzije -1.
     Primeri dostopanja :

    a = BaseArray((2, 3, 4, 5))
    a[0, 1, 2, 1] = 10 # nastavimo vrednost 10 na mesto 0, 1, 2, 1
    v = a[0, 0, 0, 0] # preberemo vrednost na mestu 0, 0, 0 ,0

    a[0, 3, 0, 0] # neveljaven indeks, ker je vrednost 3 enaka ali večja od dolžine dimenzije

    Velikost tabele lahko preberemo z lastnostjo 'shape', podatkovni tip pa z 'dtype'. Primer:

    a.shape # to vrne vrednost  (2, 3, 4, 5)
    a.dtype # vrne tip int

    Čez tabelo lahko iteriramo s for zanko, lahko uporabimo operatorje/ukaze 'reversed' in 'in'.

    a = BaseArray((4,2), data=(1, 2, 3, 4, 5, 6, 7, 8))
    for v in a: # for zanka izpiše vrednost od 1 do 8
        print(v)
    for v in reversed(a): # for zanka izpiše vrednost od 8 do 1
        print(v)

    2 in a # vrne True
    -1 in a # vrne False
    """
    def __init__(self, shape: Tuple[int], dtype: type = None, data: Union[Tuple, List] = None):
        """
        Create an initialize a multidimensional myarray of a selected data type.
        Init the myarray to 0.

        :param shape: tuple or list of integers, shape of constructed myarray
        :param dtype: type of data stored in myarray, float or int
        :param data: list of tuple of data values, must contain consistent types and
                     have a length that matches the shape.
        """

        _check_shape(shape)
        self.__shape = (*shape,)

        self.__steps = _shape_to_steps(shape)

        if dtype is None:
            dtype = float
        if dtype not in [int, float]:
            raise Exception('Type must be int or float.')
        self.__dtype = dtype

        n_elements = 1
        for s in self.__shape:
            n_elements *= s

        if data is None:
            if dtype == int:
                self.__data = array.array('i', [0]*n_elements)
            elif dtype == float:
                self.__data = array.array('f', [0]*n_elements)
        else:
            if len(data) != n_elements:
                raise Exception(f'shape {shape:} does not match provided data length {len(data):}')

            for d in data:
                if not isinstance(d, (int, float)):
                    raise Exception(f'provided data does not have a consistent type')
            self.__data = [d for d in data]

    @property
    def shape(self):
        """
        :return: a tuple representing the shape of the myarray.
        """
        return self.__shape

    @property
    def dtype(self):
        """
        :return: the type stored in the myarray.
        """
        return self.__dtype

    def __test_indice_in_range(self, ind):
        """
        Test if the given indice is in within the range of this myarray.
        :param ind: the indices used
        :return: True if indices are valid, False otherwise.
        """
        for ax in range(len(self.__shape)):
            if ind[ax] < 1 or ind[ax] > self.shape[ax]:
                raise Exception('indice {:}, axis {:d} out of bounds [1, {:}]'.format(ind, ax, self.__shape[ax]))

    def __getitem__(self, indice):
        """
        Return a value from this myarray.
        :param indice: indice to this myarray, integer or tuple
        :return: value at indice
        """
        if not isinstance(indice, tuple):
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        return self.__data[key_lin]

    def __setitem__(self, indice, value):
        """
        Set a value in this myarray.
        :param indice: valued indice to a position, integer of tuple
        :param value: value to set
        :return: does not return
        """
        if not isinstance(indice, tuple):
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)

        if self.dtype is int:
            self.__data[key_lin] = int(value)
        else:
            self.__data[key_lin] = float(value)
        
        

    def __iter__(self):
        for v in self.__data:
            yield v

    def __reversed__(self):
        for v in reversed(self.__data):
            yield v

    def __contains__(self, item):
        return item in self.__data

    def findLongestLength(self, nums):
        stlen = 1
        for st in nums:
            ll = str(st).__len__()
            if ll > stlen:
                stlen = ll
        return stlen

    def izpis(self):
        # find longest number
        l = self.shape.__len__()
        if l == 1:
            for x in range(self.shape[0]):
                print(self[x + 1], end=' ')
            print()
        elif l == 2:
            stlen = self.findLongestLength(self.__data)
            for x in range(self.shape[0]):
                for y in range(self.shape[1]):
                    # remove the > symbol to have it LEFT aligned
                    print(("{:>" + str(stlen + 1) + "}").format(str(self[x + 1, y + 1])), end='')
                print()

        elif l == 3:
            for z in range(self.shape[0]):
                print("Layer " + str(z + 1))

                stlen = self.findLongestLength(self.__data[(z*self.shape[1]*self.shape[2]):((z*self.shape[1]*self.shape[2])+(self.shape[1]*self.shape[2]))])
                for x in range(self.shape[1]):
                    for y in range(self.shape[2]):
                        # remove the > symbol to have it LEFT aligned
                        print(("{:>" + str(stlen + 1) + "}").format(str(self[z + 1, x + 1, y + 1])), end='')
                    print()
                print()
        else:
            raise ValueError("Only 1D,2D and 3D tables supported!")

    # sort table by either rows or columns using merge sort
    def sort(self, po_vrsticah = 1):
        l = self.shape.__len__()

        if l == 1:
            mergeSort(self.__data, len(self.__data))
        elif l == 2:
            if po_vrsticah == 1:
                for x in range(self.shape[0]):
                    vrstica = []

                    # get elements
                    for y in range(self.shape[1]):
                        element = self[x + 1, y + 1]
                        vrstica.append(element)

                    # sort them
                    mergeSort(vrstica, len(vrstica))

                    # insert them back in
                    for y in range(self.shape[1]):
                        self[x + 1, y + 1] = vrstica[y]
            else:
                # po stolpcih
                for y in range(self.shape[1]):
                    stolpec = []

                    # get elements
                    for x in range(self.shape[0]):
                        element = self[x + 1, y + 1]
                        stolpec.append(element)

                    # sort them
                    mergeSort(stolpec, len(stolpec))

                    # insert them back in
                    for x in range(self.shape[0]):
                        self[x + 1, y + 1] = stolpec[x]
        else:
            raise ValueError("Only 1D,2D and 3D tables supported for sorting!")

    # return all coordinates of found values that match given 'value'
    def search(self, value):
        l = self.shape.__len__()
        if l == 0 or l > 3:
            raise ValueError("Only 1D,2D and 3D tables supported!")

        foundCoordinates = []
        if l == 1:
            for x in range(self.shape[0]):
                if self[x + 1] == value:
                    foundCoordinates.append(x + 1)
        elif l == 2:
            for x in range(self.shape[0]):
                for y in range(self.shape[1]):
                    if self[x + 1,y + 1] == value:
                        foundCoordinates.append((x + 1,y + 1))
        else:
            for x in range(self.shape[0]):
                for y in range(self.shape[1]):
                    for z in range(self.shape[2]):
                        if self[x + 1,y + 1,z + 1] == value:
                            foundCoordinates.append((x + 1,y + 1,z + 1))
        return foundCoordinates

    # override addition with tables and numbers on right side
    def __add__(self, other):
        l = self.shape.__len__()
        if l == 0 or l > 3:
            raise ValueError("Only 1D,2D and 3D tables supported!")

        # IF TABLE
        if type(other) is BaseArray:
            if self.shape != other.shape:
                raise ValueError("Tables must be the same size!")

            newType = self.dtype
            if other.dtype is float:
                newType = float

            c = BaseArray(self.shape, newType)
            if l == 1:
                for x in range(self.shape[0]):
                    c[x + 1] = self[x + 1] + other[x + 1]
            elif l == 2:
                for x in range(self.shape[0]):
                    for y in range(self.shape[1]):
                        c[x + 1, y + 1] = self[x + 1, y + 1] + other[x + 1, y + 1]
            else:
                for x in range(self.shape[0]):
                    for y in range(self.shape[1]):
                        for z in range(self.shape[2]):
                            c[x + 1, y + 1, z + 1] = self[x + 1, y + 1, z + 1] + other[x + 1, y + 1, z + 1]
            return c

        # IF NUMBER
        elif type(other) is int or type(other) is float:
            c = BaseArray(self.shape, type(other))
            if l == 1:
                for x in range(self.shape[0]):
                    c[x + 1] = self[x + 1] + other
            elif l == 2:
                for x in range(self.shape[0]):
                    for y in range(self.shape[1]):
                        c[x + 1, y + 1] = self[x + 1, y + 1] + other
            else:
                for x in range(self.shape[0]):
                    for y in range(self.shape[1]):
                        for z in range(self.shape[2]):
                            c[x + 1, y + 1, z + 1] = self[x + 1, y + 1, z + 1] + other
            return c

    # override addition with numbers on left side (only for numbers)
    def __radd__(self, other):
        return self.__add__(other)

    # override subtraction with tables and numbers on right side
    def __sub__(self, other):
        l = self.shape.__len__()
        if l == 0 or l > 3:
            raise ValueError("Only 1D,2D and 3D tables supported!")

        # IF TABLE
        if type(other) is BaseArray:
            if self.shape != other.shape:
                raise ValueError("Tables must be the same size!")

            newType = self.dtype
            if other.dtype is float:
                newType = float

            c = BaseArray(self.shape, newType)
            if l == 1:
                for x in range(self.shape[0]):
                    c[x + 1] = self[x + 1] - other[x + 1]
            elif l == 2:
                for x in range(self.shape[0]):
                    for y in range(self.shape[1]):
                        c[x + 1, y + 1] = self[x + 1, y + 1] - other[x + 1, y + 1]
            else:
                for x in range(self.shape[0]):
                    for y in range(self.shape[1]):
                        for z in range(self.shape[2]):
                            c[x + 1, y + 1, z + 1] = self[x + 1, y + 1, z + 1] - other[x + 1, y + 1, z + 1]
            return c

        # IF NUMBER
        elif type(other) is int or type(other) is float:
            c = BaseArray(self.shape, type(other))
            if l == 1:
                for x in range(self.shape[0]):
                    c[x + 1] = self[x + 1] - other
            elif l == 2:
                for x in range(self.shape[0]):
                    for y in range(self.shape[1]):
                        c[x + 1, y + 1] = self[x + 1, y + 1] - other
            else:
                for x in range(self.shape[0]):
                    for y in range(self.shape[1]):
                        for z in range(self.shape[2]):
                            c[x + 1, y + 1, z + 1] = self[x + 1, y + 1, z + 1] - other
            return c

    # override subtraction from numbers on left side (only for numbers)
    def __rsub__(self, other):
        l = self.shape.__len__()
        if l == 0 or l > 3:
            raise ValueError("Only 1D,2D and 3D tables supported!")

        # IF NUMBER
        if type(other) is int or type(other) is float:
            c = BaseArray(self.shape, type(other))
            if l == 1:
                for x in range(self.shape[0]):
                    c[x + 1] = -self[x + 1] + other
            elif l == 2:
                for x in range(self.shape[0]):
                    for y in range(self.shape[1]):
                        c[x + 1, y + 1] = -self[x + 1, y + 1] + other
            else:
                for x in range(self.shape[0]):
                    for y in range(self.shape[1]):
                        for z in range(self.shape[2]):
                            c[x + 1, y + 1, z + 1] = -self[x + 1, y + 1, z + 1] + other
            return c

    # override multiplication for 2D tables and numbers
    def __mul__(self, other):
        l = self.shape.__len__()
        # IF TABLE
        if type(other) is BaseArray:
            if l != 2 or other.shape.__len__() != 2:
                raise ValueError("Only 2D tables supported for table multiplication!")

            if self.shape[1] != other.shape[0]:
                raise ValueError("Number of columns on left must match number of rows on right table!")

            newType = self.dtype
            if other.dtype is float:
                newType = float

            c = BaseArray((self.shape[0], other.shape[1]), newType)
            for i in range(self.shape[0]):
                for j in range(other.shape[1]):
                    for k in range(other.shape[0]):
                        c[i + 1, j + 1] += self[i + 1, k + 1] * other[k + 1, j + 1]
            return c

        # IF NUMBER
        elif type(other) is int or type(other) is float:
            c = BaseArray(self.shape, type(other), self.__data)
            for x in range(self.__data.__len__()):
                c.__data[x] *= other

            return c

    # override multiplication from left side (only for numbers)
    def __rmul__(self, other):
        return self.__mul__(other)

    # override negation operator
    def __neg__(self):
        return self.__mul__(-1)

    # override division operator (number on right side)
    def __truediv__(self, other):
        l = self.shape.__len__()
        if l == 0 or l > 3:
            raise ValueError("Only 1D,2D and 3D tables supported!")

        if type(other) is BaseArray:
            raise ValueError("Can not divide with tables!")

        if other == 0:
            raise ValueError("Can not divide with zero!")

        c = BaseArray(self.shape, dtype=float)
        if l == 1:
            for x in range(self.shape[0]):
                c[x + 1] = self[x + 1] / other
        elif l == 2:
            for x in range(self.shape[0]):
                for y in range(self.shape[1]):
                    c[x + 1, y + 1] = self[x + 1, y + 1] / other
        else:
            for x in range(self.shape[0]):
                for y in range(self.shape[1]):
                    for z in range(self.shape[2]):
                        c[x + 1, y + 1, z + 1] = self[x + 1, y + 1, z + 1] / other
        return c

    # converts every element into log(x) with base 'e'
    def logl(self):
        for x in range(self.__data.__len__()):
            if self.__data[x] == 0: # ignore zeros
                continue
            elif self.__data[x] < 0:
                raise ValueError("Log() does not support negative values!")
            else:
                self.__data[x] = log(self.__data[x], exp(1))

    # converts every element into e^x
    def expl(self):
        for x in range(self.__data.__len__()):
            self.__data[x] = exp(self.__data[x])

    # multiplies 2D table with itself [exponent]-times
    def pow(self, exponent):
        if exponent < 0:
            raise ValueError("Negative exponents not supported!")
        if exponent == 0:
            return 1

        c = self
        for i in range(exponent - 1):
            c *= self
        return c

# OUTER FUNCTIONS
def _check_shape(shape):
    if not isinstance(shape, (tuple, list)):
        raise Exception(f'shape {shape:} is not a tuple or list')
    for dim in shape:
        if not isinstance(dim, int):
            raise Exception(f'shape {shape:} contains a non integer {dim:}')
            
def mergeSort(alist, length):
    if length > 1:
        mid = length // 2
        lefthalf = alist[:mid]
        righthalf = alist[mid:]

        leftlen = mid
        rightlen = length - leftlen

        mergeSort(lefthalf, leftlen)
        mergeSort(righthalf, rightlen)

        i = 0
        j = 0
        k = 0
        while i < leftlen and j < rightlen:
            if lefthalf[i] < righthalf[j]:
                alist[k] = lefthalf[i]
                i = i + 1
            else:
                alist[k] = righthalf[j]
                j = j + 1
            k = k + 1

        while i < leftlen:
            alist[k] = lefthalf[i]
            i = i + 1
            k = k + 1

        while j < rightlen:
            alist[k] = righthalf[j]
            j = j + 1
            k = k + 1

def _multi_ind_iterator(multi_inds, shape):
    inds = multi_inds[0]
    s = shape[0]

    if isinstance(inds, slice):
        start, stop, step = inds.start, inds.stop, inds.step
        if start is None:
            start = 1
        if step is None:
            step = 1
        if stop is not None:
            stop += 1
        inds_itt = range(0, s+1)[start:stop:step]
    else:  # type(inds) is int:
        inds_itt = (inds,)

    for ind in inds_itt:
        if len(shape) > 1:
            for ind_tail in _multi_ind_iterator(multi_inds[1:], shape[1:]):
                yield (ind,)+ind_tail
        else:
            yield (ind,)


def _shape_to_steps(shape):
    dim_steps_rev = []
    s = 1
    for d in reversed(shape):
        dim_steps_rev.append(s)
        s *= d
    steps = tuple(reversed(dim_steps_rev))
    return steps


def _multi_to_lin_ind(inds, steps):
    i = 0
    for n, s in enumerate(steps):
        i += (inds[n]-1)*s
    return i


def _is_valid_indice(indice):
    if isinstance(indice, tuple):
        if not indice:
            return False
        for i in indice:
            if not isinstance(i, int):
                return False
    elif not isinstance(indice, int):
        return False
    return True
