from basearray import mergeSort
import cProfile
import random

# printing 2D and 3D - test with ints, floats, negative, positive numbers - check alignments

count = 300000
max = 100000
min = -max
list = []

# generate numbers for list
for i in range(0, count):
    list.append(random.randint(min, max))

# profile 'mergeSort'
cProfile.run('mergeSort(list, len(list))')


# ncalls = number of calls
# tottime = total time spent in the given function (and excluding time made in calls to sub-functions)
# percall = quotient of 'tottime' divided by 'ncalls'
# cumtime = s the cumulative time spent in this and all subfunctions (from invocation till exit). This figure is accurate even for recursive functions.
# percall = quotient of 'cumtime' divided by primitive calls