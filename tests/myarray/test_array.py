from unittest import TestCase
from myarray.basearray import BaseArray
import myarray.basearray as ndarray
import random
from math import log, exp
import io
import sys


class TestArray(TestCase):
    def test___init__(self):

        # simple init tests, other things are tested later
        try:
            a = BaseArray((2,))
            a = BaseArray([2, 5])
            a = BaseArray((2, 5), dtype=int)
            a = BaseArray((2, 5), dtype=float)
        except Exception as e:
            self.fail(f'basic constructor test failed, with exception {e:}')

        # test invalid parameters as shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray(5)
        self.assertEqual(('shape 5 is not a tuple or list',),
                         cm.exception.args)

        # test invalid value in shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray((4, 'a'))
        self.assertEqual(('shape (4, \'a\') contains a non integer a',),
                         cm.exception.args)

        # TODO test invalid dtype
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 5), dtype=BaseArray)
        self.assertEqual('Type must be int or float.', cm.exception.args[0])

        # test valid data param, list, tuple
        a = BaseArray((3, 4), dtype=int, data=list(range(12)))
        a = BaseArray((3, 4), dtype=int, data=tuple(range(12)))

        # test invalid data param
        # invalid data length
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 4), dtype=int, data=list(range(12)))
        self.assertEqual(('shape (2, 4) does not match provided data length 12',),
                         cm.exception.args)

        # inconsistent data type
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 2), dtype=int, data=(4, 'a', (), 2.0))
        self.assertEqual(('provided data does not have a consistent type',),
                         cm.exception.args)

    def test_shape(self):
        a = BaseArray((5,))
        self.assertTupleEqual(a.shape, (5,))
        a = BaseArray((2, 5))
        self.assertTupleEqual(a.shape, (2, 5))
        a = BaseArray((3, 2, 5))
        self.assertEqual(a.shape, (3, 2, 5))

        # random test
        i1 = random.randint(1, 20)
        i2 = random.randint(1, 20)
        i3 = random.randint(1, 20)
        a = BaseArray((i1, i2, i3))
        self.assertEqual(a.shape, (i1, i2, i3))

    def test_dtype(self):
        # test if setting a type returns the set type
        a = BaseArray((1,))
        self.assertIsInstance(a[1], float)
        a = BaseArray((1,), int)
        self.assertIsInstance(a[1], int)
        a = BaseArray((1,), float)
        self.assertIsInstance(a[1], float)

    def test_get_set_item(self):
        # test if setting a value returns the value
        a = BaseArray((1,))
        a[1] = 1
        self.assertEqual(1, a[1])
        # test for multiple dimensions
        a = BaseArray((2, 1))
        a[1, 1] = 1
        a[2, 1] = 1
        self.assertEqual(1, a[1, 1])
        self.assertEqual(1, a[2, 1])

        # test with invalid indices
        a = BaseArray((2, 2))
        with self.assertRaises(Exception) as cm:
            a[4, 1] = 0
        self.assertEqual(('indice (4, 1), axis 0 out of bounds [1, 2]',),
                         cm.exception.args)

        # test invalid type of ind
        with self.assertRaises(Exception) as cm:
            a[1, 1.1] = 0
        self.assertEqual(cm.exception.args,
                         ('(1, 1.1) is not a valid indice',))

        with self.assertRaises(Exception) as cm:
            a[1, 'a'] = 0
        self.assertEqual(('(1, \'a\') is not a valid indice',),
                         cm.exception.args)

        # test invalid number of ind
        with self.assertRaises(Exception) as cm:
            a[2] = 0
        self.assertEqual(('indice (2,) is not valid for this myarray',),
                         cm.exception.args)

        # test data intialized via data parameter
        a = BaseArray((2, 3, 4), dtype=int, data=tuple(range(24)))
        self.assertEqual(3, a[1, 1, 4])
        self.assertEqual(13, a[2, 1, 2])

        # TODO enter invalid type

    def test_iter(self):
        a = BaseArray((2, 3), dtype=int)
        a[1, 1] = 1
        a[1, 2] = 2
        a[1, 3] = 3
        a[2, 1] = 4
        a[2, 2] = 5
        a[2, 3] = 6

        a_vals = [v for v in a]
        self.assertListEqual([1, 2, 3, 4, 5, 6], a_vals)

    def test_reversed(self):
        a = BaseArray((2, 3), dtype=int)
        a[1, 1] = 1
        a[1, 2] = 2
        a[1, 3] = 3
        a[2, 1] = 4
        a[2, 2] = 5
        a[2, 3] = 6

        a_vals = [v for v in reversed(a)]
        self.assertListEqual([6, 5, 4, 3, 2, 1], a_vals)

    def test_contains(self):
        a = BaseArray((1,), dtype=float)
        a[1] = 1

        self.assertIn(1., a)
        self.assertIn(1, a)
        self.assertNotIn(0, a)

    def test_shape_to_steps(self):
        shape = (4, 2, 3)
        steps_exp = (6, 3, 1)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = [1]
        steps_exp = (1,)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = []
        steps_exp = ()
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

    def test_multi_to_lin_ind(self):
        # shape = 4, 2,
        # 0 1
        # 2 3
        # 4 5
        # 6 7
        steps = 2, 1
        multi_lin_inds_pairs = (((1, 1), 0),
                                ((2, 2), 3),
                                ((4, 1), 6),
                                ((4, 2), 7))
        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_exp, lin_ind_res)

        # shape = 2, 3, 4
        #
        # 0 1 2 3
        # 4 5 6 7
        #
        #  8  9 10 11
        # 12 13 14 15
        steps = 8, 4, 1
        multi_lin_inds_pairs = (((1, 1, 1), 0),
                                ((2, 2, 1), 12),
                                ((1, 1, 4), 3),
                                ((2, 2, 3), 14))

        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_res, lin_ind_exp)

    def test_is_valid_instance(self):
        self.assertTrue(ndarray._is_valid_indice(2))
        self.assertTrue(ndarray._is_valid_indice((2, 1)))

        self.assertFalse(ndarray._is_valid_indice(()))
        self.assertFalse(ndarray._is_valid_indice([2]))
        self.assertFalse(ndarray._is_valid_indice(2.0))
        self.assertFalse(ndarray._is_valid_indice((2, 3.)))

    def test_multi_ind_iterator(self):
        shape = (30,)
        ind = (slice(10, 20, 3),)
        ind_list_exp = ((10,),
                        (13,),
                        (16,),
                        (19,))
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        shape = (5, 5, 5)
        ind = (2, 2, 2)
        ind_list_exp = ((2, 2, 2),)
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (2, slice(3), 2)
        ind_list_exp = ((2, 1, 2),
                        (2, 2, 2),
                        (2, 3, 2),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (slice(None, None, 2), slice(3), slice(2, 4))
        ind_list_exp = ((1, 1, 2), (1, 1, 3), (1, 1, 4),
                        (1, 2, 2), (1, 2, 3), (1, 2, 4),
                        (1, 3, 2), (1, 3, 3), (1, 3, 4),
                        (3, 1, 2), (3, 1, 3), (3, 1, 4),
                        (3, 2, 2), (3, 2, 3), (3, 2, 4),
                        (3, 3, 2), (3, 3, 3), (3, 3, 4),
                        (5, 1, 2), (5, 1, 3), (5, 1, 4),
                        (5, 2, 2), (5, 2, 3), (5, 2, 4),
                        (5, 3, 2), (5, 3, 3), (5, 3, 4),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)


    def test_sorting_1d(self):
        a = BaseArray((5,))
        a[1] = 1
        a[2] = -1
        a[3] = -1
        a[4] = 5
        a[5] = 4

        # sorting by rows
        a.sort(1)        

        self.assertEqual(a[1], -1)
        self.assertEqual(a[2], -1)
        self.assertEqual(a[3], 1)
        self.assertEqual(a[4], 4)
        self.assertEqual(a[5], 5)

        # sorting by columns - should be the same for 1D tables!
        a.sort(0)

        self.assertEqual(a[1], -1)
        self.assertEqual(a[2], -1)
        self.assertEqual(a[3], 1)
        self.assertEqual(a[4], 4)
        self.assertEqual(a[5], 5)

        # test no elements
        a = BaseArray((0,))
        a.sort(1)

    def test_sorting_2d(self):
        # sorting 2D array by rows
        a = BaseArray((3,3))
        a[1, 1] = 5
        a[1, 2] = 5
        a[1, 3] = 5
        a[2, 1] = -4
        a[2, 2] = -5
        a[2, 3] = 50
        a[3, 1] = 0
        a[3, 2] = 1
        a[3, 3] = -1
        a.sort(1)

        self.assertEqual(a[1, 1], 5)
        self.assertEqual(a[1, 2], 5)
        self.assertEqual(a[1, 3], 5)
        self.assertEqual(a[2, 1], -5)
        self.assertEqual(a[2, 2], -4)
        self.assertEqual(a[2, 3], 50)
        self.assertEqual(a[3, 1], -1)
        self.assertEqual(a[3, 2], 0)
        self.assertEqual(a[3, 3], 1)

        # sorting 2D array by columns
        a.sort(0)

        self.assertEqual(a[1, 1], -5)
        self.assertEqual(a[1, 2], -4)
        self.assertEqual(a[1, 3], 1)
        self.assertEqual(a[2, 1], -1)
        self.assertEqual(a[2, 2], 0)
        self.assertEqual(a[2, 3], 5)
        self.assertEqual(a[3, 1], 5)
        self.assertEqual(a[3, 2], 5)
        self.assertEqual(a[3, 3], 50)

    def test_sorting_invalid(self):
        a = BaseArray((3,3,3,3))
        with self.assertRaises(Exception) as cm:
            a.sort()
        self.assertEqual('Only 1D,2D and 3D tables supported for sorting!', cm.exception.args[0])

    def test_search_1d(self):
        a = BaseArray((7,))
        a[1] = 1
        a[2] = 13
        a[3] = -3
        a[4] = 4
        a[5] = 67
        a[6] = 67
        a[7] = 0

        # find single value
        results = a.search(4)
        self.assertEqual(results, [4])

        # find multiple values
        results = a.search(67)
        self.assertEqual(results, [5, 6])

        # find missing value
        results = a.search(60)
        self.assertEqual(results, [])

    def test_search_2d(self):
        a = BaseArray((3,3))
        a[1, 1] = 5
        a[1, 2] = 5
        a[1, 3] = 5
        a[2, 1] = -4
        a[2, 2] = -5
        a[2, 3] = 50
        a[3, 1] = 0
        a[3, 2] = 1
        a[3, 3] = -1

        # find single value
        results = a.search(1)
        self.assertEqual(results, [(3, 2)])

        # find multiple values
        results = a.search(5)
        self.assertEqual(results, [(1, 1), (1, 2), (1, 3)])

        # find missing value
        results = a.search(9)
        self.assertEqual(results, [])

    def test_search_3d(self):
        a = BaseArray((3,3,3))
        a[1, 1, 2] = 5
        a[1, 2, 1] = 5
        a[1, 3, 3] = 5
        a[2, 1, 2] = -4
        a[2, 2, 1] = -5
        a[2, 3, 2] = 50
        a[3, 1, 2] = 0
        a[3, 2, 2] = 1
        a[3, 3, 1] = -1

        # find single value
        results = a.search(1)
        self.assertEqual(results, [(3, 2, 2)])

        # find multiple values
        results = a.search(5)
        self.assertEqual(results, [(1, 1, 2), (1, 2, 1), (1, 3, 3)])

        # find missing value
        results = a.search(9)
        self.assertEqual(results, [])

    def test_search_invalid(self):
        a = BaseArray((3, 3, 3, 3))
        with self.assertRaises(Exception) as cm:
            a.search(0)
        self.assertEqual('Only 1D,2D and 3D tables supported!', cm.exception.args[0])

    def test_addition_1d(self):
        a = BaseArray((3,), int)
        a[1] = -5
        a[2] = 1
        a[3] = 1

        b = BaseArray((3,), float)
        b[1] = 4.3
        b[2] = 3.1
        b[3] = 2.5

        # also test data types
        self.assertEqual(a.dtype, int)
        self.assertEqual(b.dtype, float)

        # test adding tables together from both sides
        # also check if data type persists or changes based on the other element
        c = a + b
        self.assertEqual(c[1], a[1] + b[1])
        self.assertEqual(c[2], a[2] + b[2])
        self.assertEqual(c[3], a[3] + b[3])
        self.assertEqual(c.dtype, float)

        c = b + a
        self.assertEqual(c[1], a[1] + b[1])
        self.assertEqual(c[2], a[2] + b[2])
        self.assertEqual(c[3], a[3] + b[3])
        self.assertEqual(c.dtype, float)

        c = a + a
        self.assertEqual(c[1], a[1] + a[1])
        self.assertEqual(c[2], a[2] + a[2])
        self.assertEqual(c[3], a[3] + a[3])
        self.assertEqual(c.dtype, int)

        # test adding numbers from both sides
        c = a + 1
        self.assertEqual(c[1], a[1] + 1)
        self.assertEqual(c[2], a[2] + 1)
        self.assertEqual(c[3], a[3] + 1)
        self.assertEqual(c.dtype, int)

        c = 1 + a
        self.assertEqual(c[1], a[1] + 1)
        self.assertEqual(c[2], a[2] + 1)
        self.assertEqual(c[3], a[3] + 1)
        self.assertEqual(c.dtype, int)

        c = a + 1.5
        self.assertEqual(c[1], a[1] + 1.5)
        self.assertEqual(c[2], a[2] + 1.5)
        self.assertEqual(c[3], a[3] + 1.5)
        self.assertEqual(c.dtype, float)

        c = 1.5 + a
        self.assertEqual(c[1], a[1] + 1.5)
        self.assertEqual(c[2], a[2] + 1.5)
        self.assertEqual(c[3], a[3] + 1.5)
        self.assertEqual(c.dtype, float)

    def test_addition_2d(self):
        a = BaseArray((3,3), int)
        a[1, 1] = -5
        a[1, 2] = 1
        a[1, 3] = 1
        a[2, 1] = 0
        a[2, 2] = 3
        a[2, 3] = 4
        a[3, 1] = 233
        a[3, 2] = 43
        a[3, 3] = 123

        b = BaseArray((3, 3), float)
        b[1, 1] = 5
        b[1, 2] = 6.5
        b[1, 3] = 6
        b[2, 1] = -45.76
        b[2, 2] = 3.13
        b[2, 3] = 43
        b[3, 1] = 2
        b[3, 2] = 4.98
        b[3, 3] = 12

        # also test data types
        self.assertEqual(a.dtype, int)
        self.assertEqual(b.dtype, float)

        # test adding tables together from both sides
        # also check if data type persists or changes based on the other element
        c = a + b
        self.assertAlmostEqual(c[1, 1], a[1, 1] + b[1, 1], 2)
        self.assertAlmostEqual(c[1, 2], a[1, 2] + b[1, 2], 2)
        self.assertAlmostEqual(c[1, 3], a[1, 3] + b[1, 3], 2)
        self.assertAlmostEqual(c[2, 1], a[2, 1] + b[2, 1], 2)
        self.assertAlmostEqual(c[2, 2], a[2, 2] + b[2, 2], 2)
        self.assertAlmostEqual(c[2, 3], a[2, 3] + b[2, 3], 2)
        self.assertAlmostEqual(c[3, 1], a[3, 1] + b[3, 1], 2)
        self.assertAlmostEqual(c[3, 2], a[3, 2] + b[3, 2], 2)
        self.assertAlmostEqual(c[3, 3], a[3, 3] + b[3, 3], 2)
        self.assertEqual(c.dtype, float)

        c = b + a
        self.assertAlmostEqual(c[1, 1], a[1, 1] + b[1, 1], 2)
        self.assertAlmostEqual(c[1, 2], a[1, 2] + b[1, 2], 2)
        self.assertAlmostEqual(c[1, 3], a[1, 3] + b[1, 3], 2)
        self.assertAlmostEqual(c[2, 1], a[2, 1] + b[2, 1], 2)
        self.assertAlmostEqual(c[2, 2], a[2, 2] + b[2, 2], 2)
        self.assertAlmostEqual(c[2, 3], a[2, 3] + b[2, 3], 2)
        self.assertAlmostEqual(c[3, 1], a[3, 1] + b[3, 1], 2)
        self.assertAlmostEqual(c[3, 2], a[3, 2] + b[3, 2], 2)
        self.assertAlmostEqual(c[3, 3], a[3, 3] + b[3, 3], 2)
        self.assertEqual(c.dtype, float)

        c = a + a
        self.assertAlmostEqual(c[1, 1], a[1, 1] + a[1, 1], 2)
        self.assertAlmostEqual(c[1, 2], a[1, 2] + a[1, 2], 2)
        self.assertAlmostEqual(c[1, 3], a[1, 3] + a[1, 3], 2)
        self.assertAlmostEqual(c[2, 1], a[2, 1] + a[2, 1], 2)
        self.assertAlmostEqual(c[2, 2], a[2, 2] + a[2, 2], 2)
        self.assertAlmostEqual(c[2, 3], a[2, 3] + a[2, 3], 2)
        self.assertAlmostEqual(c[3, 1], a[3, 1] + a[3, 1], 2)
        self.assertAlmostEqual(c[3, 2], a[3, 2] + a[3, 2], 2)
        self.assertAlmostEqual(c[3, 3], a[3, 3] + a[3, 3], 2)
        self.assertEqual(c.dtype, int)

        # test adding numbers from both sides
        c = a + 1
        self.assertAlmostEqual(c[1, 1], a[1, 1] + 1, 2)
        self.assertAlmostEqual(c[1, 2], a[1, 2] + 1, 2)
        self.assertAlmostEqual(c[1, 3], a[1, 3] + 1, 2)
        self.assertAlmostEqual(c[2, 1], a[2, 1] + 1, 2)
        self.assertAlmostEqual(c[2, 2], a[2, 2] + 1, 2)
        self.assertAlmostEqual(c[2, 3], a[2, 3] + 1, 2)
        self.assertAlmostEqual(c[3, 1], a[3, 1] + 1, 2)
        self.assertAlmostEqual(c[3, 2], a[3, 2] + 1, 2)
        self.assertAlmostEqual(c[3, 3], a[3, 3] + 1, 2)
        self.assertEqual(c.dtype, int)

        c = 1 + a
        self.assertAlmostEqual(c[1, 1], a[1, 1] + 1, 2)
        self.assertAlmostEqual(c[1, 2], a[1, 2] + 1, 2)
        self.assertAlmostEqual(c[1, 3], a[1, 3] + 1, 2)
        self.assertAlmostEqual(c[2, 1], a[2, 1] + 1, 2)
        self.assertAlmostEqual(c[2, 2], a[2, 2] + 1, 2)
        self.assertAlmostEqual(c[2, 3], a[2, 3] + 1, 2)
        self.assertAlmostEqual(c[3, 1], a[3, 1] + 1, 2)
        self.assertAlmostEqual(c[3, 2], a[3, 2] + 1, 2)
        self.assertAlmostEqual(c[3, 3], a[3, 3] + 1, 2)
        self.assertEqual(c.dtype, int)

        c = a + 1.5
        self.assertAlmostEqual(c[1, 1], a[1, 1] + 1.5, 2)
        self.assertAlmostEqual(c[1, 2], a[1, 2] + 1.5, 2)
        self.assertAlmostEqual(c[1, 3], a[1, 3] + 1.5, 2)
        self.assertAlmostEqual(c[2, 1], a[2, 1] + 1.5, 2)
        self.assertAlmostEqual(c[2, 2], a[2, 2] + 1.5, 2)
        self.assertAlmostEqual(c[2, 3], a[2, 3] + 1.5, 2)
        self.assertAlmostEqual(c[3, 1], a[3, 1] + 1.5, 2)
        self.assertAlmostEqual(c[3, 2], a[3, 2] + 1.5, 2)
        self.assertAlmostEqual(c[3, 3], a[3, 3] + 1.5, 2)
        self.assertEqual(c.dtype, float)

        c = 1.5 + a
        self.assertAlmostEqual(c[1, 1], a[1, 1] + 1.5, 2)
        self.assertAlmostEqual(c[1, 2], a[1, 2] + 1.5, 2)
        self.assertAlmostEqual(c[1, 3], a[1, 3] + 1.5, 2)
        self.assertAlmostEqual(c[2, 1], a[2, 1] + 1.5, 2)
        self.assertAlmostEqual(c[2, 2], a[2, 2] + 1.5, 2)
        self.assertAlmostEqual(c[2, 3], a[2, 3] + 1.5, 2)
        self.assertAlmostEqual(c[3, 1], a[3, 1] + 1.5, 2)
        self.assertAlmostEqual(c[3, 2], a[3, 2] + 1.5, 2)
        self.assertAlmostEqual(c[3, 3], a[3, 3] + 1.5, 2)
        self.assertEqual(c.dtype, float)

    def test_addition_3d(self):
        # quick 3d test
        a = BaseArray((2,2,2))
        a[1, 1, 1] = 1
        a[2, 2, 2] = 1

        b = BaseArray((2, 2, 2))
        b[1, 1, 1] = 4
        b[2, 2, 2] = -4.5

        c = a + b
        self.assertEqual(c[1, 1, 1], a[1, 1, 1] + b[1, 1, 1])
        self.assertEqual(c[2, 2, 2], a[2, 2, 2] + b[2, 2, 2])
        self.assertEqual(c[1, 2, 1], a[1, 2, 1] + b[1, 2, 1])
        # and so on...

        c = b + a
        self.assertEqual(c[1, 1, 1], a[1, 1, 1] + b[1, 1, 1])
        self.assertEqual(c[2, 2, 2], a[2, 2, 2] + b[2, 2, 2])
        self.assertEqual(c[1, 2, 1], a[1, 2, 1] + b[1, 2, 1])
        # and so on...

        c = a + 1
        self.assertEqual(c[1, 1, 1], a[1, 1, 1] + 1)
        self.assertEqual(c[2, 2, 2], a[2, 2, 2] + 1)
        self.assertEqual(c[1, 2, 1], a[1, 2, 1] + 1)
        # and so on...

        c = 1 + a
        self.assertEqual(c[1, 1, 1], a[1, 1, 1] + 1)
        self.assertEqual(c[2, 2, 2], a[2, 2, 2] + 1)
        self.assertEqual(c[1, 2, 1], a[1, 2, 1] + 1)
        # and so on...

    def test_addition_invalid(self):
        # adding tables of different sizes must raise exception
        a = BaseArray((3,), int)
        a[1] = -5
        a[2] = 1
        a[3] = 1

        b = BaseArray((3,3,3,3))
        with self.assertRaises(Exception) as cm:
            c = a + b

        self.assertEqual('Tables must be the same size!', cm.exception.args[0])

    def test_subtraction_1d(self):
        a = BaseArray((3,), int)
        a[1] = -5
        a[2] = 1
        a[3] = 1

        b = BaseArray((3,), float)
        b[1] = 4.3
        b[2] = 3.1
        b[3] = 2.5

        # also test data types
        self.assertEqual(a.dtype, int)
        self.assertEqual(b.dtype, float)

        # test adding tables together from both sides
        # also check if data type persists or changes based on the other element
        c = a - b
        self.assertEqual(c[1], a[1] - b[1])
        self.assertEqual(c[2], a[2] - b[2])
        self.assertEqual(c[3], a[3] - b[3])
        self.assertEqual(c.dtype, float)

        c = b - a
        self.assertEqual(c[1], b[1] - a[1])
        self.assertEqual(c[2], b[2] - a[2])
        self.assertEqual(c[3], b[3] - a[3])
        self.assertEqual(c.dtype, float)

        c = a - a
        self.assertEqual(c[1], 0)
        self.assertEqual(c[2], 0)
        self.assertEqual(c[3], 0)
        self.assertEqual(c.dtype, int)

        # test adding numbers from both sides
        c = a - 1
        self.assertEqual(c[1], a[1] - 1)
        self.assertEqual(c[2], a[2] - 1)
        self.assertEqual(c[3], a[3] - 1)
        self.assertEqual(c.dtype, int)

        c = 1 - a
        self.assertEqual(c[1], 1 - a[1])
        self.assertEqual(c[2], 1 - a[2])
        self.assertEqual(c[3], 1 - a[3])
        self.assertEqual(c.dtype, int)

        c = a - 1.5
        self.assertEqual(c[1], a[1] - 1.5)
        self.assertEqual(c[2], a[2] - 1.5)
        self.assertEqual(c[3], a[3] - 1.5)
        self.assertEqual(c.dtype, float)

        c = 1.5 - a
        self.assertEqual(c[1], 1.5 - a[1])
        self.assertEqual(c[2], 1.5 - a[2])
        self.assertEqual(c[3], 1.5 - a[3])
        self.assertEqual(c.dtype, float)

    def test_subtraction_2d(self):
        a = BaseArray((3, 3), int)
        a[1, 1] = -5
        a[1, 2] = 1
        a[1, 3] = 1
        a[2, 1] = 0
        a[2, 2] = 3
        a[2, 3] = 4
        a[3, 1] = 233
        a[3, 2] = 43
        a[3, 3] = 123

        b = BaseArray((3, 3), float)
        b[1, 1] = 5
        b[1, 2] = 6.5
        b[1, 3] = 6
        b[2, 1] = -45.76
        b[2, 2] = 3.13
        b[2, 3] = 43
        b[3, 1] = 2
        b[3, 2] = 4.98
        b[3, 3] = 12

        # also test data types
        self.assertEqual(a.dtype, int)
        self.assertEqual(b.dtype, float)

        # test adding tables together from both sides
        # also check if data type persists or changes based on the other element
        c = a - b
        self.assertAlmostEqual(c[1, 1], a[1, 1] - b[1, 1], 2)
        self.assertAlmostEqual(c[1, 2], a[1, 2] - b[1, 2], 2)
        self.assertAlmostEqual(c[1, 3], a[1, 3] - b[1, 3], 2)
        self.assertAlmostEqual(c[2, 1], a[2, 1] - b[2, 1], 2)
        self.assertAlmostEqual(c[2, 2], a[2, 2] - b[2, 2], 2)
        self.assertAlmostEqual(c[2, 3], a[2, 3] - b[2, 3], 2)
        self.assertAlmostEqual(c[3, 1], a[3, 1] - b[3, 1], 2)
        self.assertAlmostEqual(c[3, 2], a[3, 2] - b[3, 2], 2)
        self.assertAlmostEqual(c[3, 3], a[3, 3] - b[3, 3], 2)
        self.assertEqual(c.dtype, float)

        c = b - a
        self.assertAlmostEqual(c[1, 1], b[1, 1] - a[1, 1], 2)
        self.assertAlmostEqual(c[1, 2], b[1, 2] - a[1, 2], 2)
        self.assertAlmostEqual(c[1, 3], b[1, 3] - a[1, 3], 2)
        self.assertAlmostEqual(c[2, 1], b[2, 1] - a[2, 1], 2)
        self.assertAlmostEqual(c[2, 2], b[2, 2] - a[2, 2], 2)
        self.assertAlmostEqual(c[2, 3], b[2, 3] - a[2, 3], 2)
        self.assertAlmostEqual(c[3, 1], b[3, 1] - a[3, 1], 2)
        self.assertAlmostEqual(c[3, 2], b[3, 2] - a[3, 2], 2)
        self.assertAlmostEqual(c[3, 3], b[3, 3] - a[3, 3], 2)
        self.assertEqual(c.dtype, float)

        c = a - a
        self.assertAlmostEqual(c[1, 1], 0, 2)
        self.assertAlmostEqual(c[1, 2], 0, 2)
        self.assertAlmostEqual(c[1, 3], 0, 2)
        self.assertAlmostEqual(c[2, 1], 0, 2)
        self.assertAlmostEqual(c[2, 2], 0, 2)
        self.assertAlmostEqual(c[2, 3], 0, 2)
        self.assertAlmostEqual(c[3, 1], 0, 2)
        self.assertAlmostEqual(c[3, 2], 0, 2)
        self.assertAlmostEqual(c[3, 3], 0, 2)
        self.assertEqual(c.dtype, int)

        # test adding numbers from both sides
        c = a - 1
        self.assertAlmostEqual(c[1, 1], a[1, 1] - 1, 2)
        self.assertAlmostEqual(c[1, 2], a[1, 2] - 1, 2)
        self.assertAlmostEqual(c[1, 3], a[1, 3] - 1, 2)
        self.assertAlmostEqual(c[2, 1], a[2, 1] - 1, 2)
        self.assertAlmostEqual(c[2, 2], a[2, 2] - 1, 2)
        self.assertAlmostEqual(c[2, 3], a[2, 3] - 1, 2)
        self.assertAlmostEqual(c[3, 1], a[3, 1] - 1, 2)
        self.assertAlmostEqual(c[3, 2], a[3, 2] - 1, 2)
        self.assertAlmostEqual(c[3, 3], a[3, 3] - 1, 2)
        self.assertEqual(c.dtype, int)

        c = 1 - a
        self.assertAlmostEqual(c[1, 1], 1 - a[1, 1], 2)
        self.assertAlmostEqual(c[1, 2], 1 - a[1, 2], 2)
        self.assertAlmostEqual(c[1, 3], 1 - a[1, 3], 2)
        self.assertAlmostEqual(c[2, 1], 1 - a[2, 1], 2)
        self.assertAlmostEqual(c[2, 2], 1 - a[2, 2], 2)
        self.assertAlmostEqual(c[2, 3], 1 - a[2, 3], 2)
        self.assertAlmostEqual(c[3, 1], 1 - a[3, 1], 2)
        self.assertAlmostEqual(c[3, 2], 1 - a[3, 2], 2)
        self.assertAlmostEqual(c[3, 3], 1 - a[3, 3], 2)
        self.assertEqual(c.dtype, int)

        c = a - 1.5
        self.assertAlmostEqual(c[1, 1], a[1, 1] - 1.5, 2)
        self.assertAlmostEqual(c[1, 2], a[1, 2] - 1.5, 2)
        self.assertAlmostEqual(c[1, 3], a[1, 3] - 1.5, 2)
        self.assertAlmostEqual(c[2, 1], a[2, 1] - 1.5, 2)
        self.assertAlmostEqual(c[2, 2], a[2, 2] - 1.5, 2)
        self.assertAlmostEqual(c[2, 3], a[2, 3] - 1.5, 2)
        self.assertAlmostEqual(c[3, 1], a[3, 1] - 1.5, 2)
        self.assertAlmostEqual(c[3, 2], a[3, 2] - 1.5, 2)
        self.assertAlmostEqual(c[3, 3], a[3, 3] - 1.5, 2)
        self.assertEqual(c.dtype, float)

        c = 1.5 - a
        self.assertAlmostEqual(c[1, 1], 1.5 - a[1, 1], 2)
        self.assertAlmostEqual(c[1, 2], 1.5 - a[1, 2], 2)
        self.assertAlmostEqual(c[1, 3], 1.5 - a[1, 3], 2)
        self.assertAlmostEqual(c[2, 1], 1.5 - a[2, 1], 2)
        self.assertAlmostEqual(c[2, 2], 1.5 - a[2, 2], 2)
        self.assertAlmostEqual(c[2, 3], 1.5 - a[2, 3], 2)
        self.assertAlmostEqual(c[3, 1], 1.5 - a[3, 1], 2)
        self.assertAlmostEqual(c[3, 2], 1.5 - a[3, 2], 2)
        self.assertAlmostEqual(c[3, 3], 1.5 - a[3, 3], 2)
        self.assertEqual(c.dtype, float)

    def test_subtraction_3d(self):
        # quick 3d test
        a = BaseArray((2, 2, 2))
        a[1, 1, 1] = 1
        a[2, 2, 2] = 1

        b = BaseArray((2, 2, 2))
        b[1, 1, 1] = 4
        b[2, 2, 2] = -4.5

        c = a - b
        self.assertEqual(c[1, 1, 1], a[1, 1, 1] - b[1, 1, 1])
        self.assertEqual(c[2, 2, 2], a[2, 2, 2] - b[2, 2, 2])
        self.assertEqual(c[1, 2, 1], a[1, 2, 1] - b[1, 2, 1])
        # and so on...

        c = b - a
        self.assertEqual(c[1, 1, 1], b[1, 1, 1] - a[1, 1, 1])
        self.assertEqual(c[2, 2, 2], b[2, 2, 2] - a[2, 2, 2])
        self.assertEqual(c[1, 2, 1], b[1, 2, 1] - a[1, 2, 1])
        # and so on...

        c = a - 1
        self.assertEqual(c[1, 1, 1], a[1, 1, 1] - 1)
        self.assertEqual(c[2, 2, 2], a[2, 2, 2] - 1)
        self.assertEqual(c[1, 2, 1], a[1, 2, 1] - 1)
        # and so on...

        c = 1 - a
        self.assertEqual(c[1, 1, 1], 1 - a[1, 1, 1])
        self.assertEqual(c[2, 2, 2], 1 - a[2, 2, 2])
        self.assertEqual(c[1, 2, 1], 1 - a[1, 2, 1])
        # and so on...

    def test_subtraction_invalid(self):
        # adding tables of different sizes must raise exception
        a = BaseArray((3,), int)
        a[1] = -5
        a[2] = 1
        a[3] = 1

        b = BaseArray((3, 3, 3, 3))
        with self.assertRaises(Exception) as cm:
            c = a - b

        self.assertEqual('Tables must be the same size!', cm.exception.args[0])

    def test_neg(self):
        a = BaseArray((3,))
        a[1] = 2
        a[2] = 3
        a[3] = -4

        c = -a
        self.assertEqual(c[1], -a[1])
        self.assertEqual(c[2], -a[2])
        self.assertEqual(c[3], -a[3])

    def test_division_1d(self):
        a = BaseArray((3,), int)
        a[1] = -5
        a[2] = 1
        a[3] = 1

        c = a / 1
        self.assertEqual(c[1], a[1])
        self.assertEqual(c[2], a[2])
        self.assertEqual(c[3], a[3])
        self.assertEqual(c.dtype, float)

        c = a / 2
        self.assertEqual(c[1], a[1] / 2)
        self.assertEqual(c[2], a[2] / 2)
        self.assertEqual(c[3], a[3] / 2)
        self.assertEqual(c.dtype, float)

    def test_division_2d(self):
        a = BaseArray((3,3), int)
        a[1, 1] = -5
        a[1, 2] = 1
        a[1, 3] = 1
        a[2, 1] = 0
        a[2, 2] = 3
        a[2, 3] = 4
        a[3, 1] = 233
        a[3, 2] = 43
        a[3, 3] = 123

        c = a / 1
        self.assertEqual(c[1, 1], a[1, 1])
        self.assertEqual(c[1, 2], a[1, 2])
        self.assertEqual(c[1, 3], a[1, 3])
        self.assertEqual(c[2, 1], a[2, 1])
        self.assertEqual(c[2, 2], a[2, 2])
        self.assertEqual(c[2, 3], a[2, 3])
        self.assertEqual(c[3, 1], a[3, 1])
        self.assertEqual(c[3, 2], a[3, 2])
        self.assertEqual(c[3, 3], a[3, 3])
        self.assertEqual(c.dtype, float)

        c = a / 2
        self.assertEqual(c[1, 1], a[1, 1] / 2)
        self.assertEqual(c[1, 2], a[1, 2] / 2)
        self.assertEqual(c[1, 3], a[1, 3] / 2)
        self.assertEqual(c[2, 1], a[2, 1] / 2)
        self.assertEqual(c[2, 2], a[2, 2] / 2)
        self.assertEqual(c[2, 3], a[2, 3] / 2)
        self.assertEqual(c[3, 1], a[3, 1] / 2)
        self.assertEqual(c[3, 2], a[3, 2] / 2)
        self.assertEqual(c[3, 3], a[3, 3] / 2)
        self.assertEqual(c.dtype, float)

    def test_division_3d(self):
        # quick 3d test
        a = BaseArray((2, 2, 2))
        a[1, 1, 1] = 1
        a[2, 2, 2] = 1

        c = a / 2
        self.assertEqual(c[1, 1, 1], a[1, 1, 1] / 2)
        self.assertEqual(c[2, 2, 2], a[2, 2, 2] / 2)
        self.assertEqual(c[1, 2, 1], a[1, 2, 1] / 2)
        # and so on...

    def test_division_invalid(self):

        # divide with zero
        a = BaseArray((3,))
        a[1] = 1.4
        a[2] = 3
        a[3] = 4.5

        with self.assertRaises(Exception) as cm:
            c = a / 0
        self.assertEqual('Can not divide with zero!', cm.exception.args[0])

        # divide table with table
        b = BaseArray((3,))
        b[1] = 1.4
        b[2] = 3
        b[3] = 4.5

        with self.assertRaises(Exception) as cm:
            c = a / b
        self.assertEqual('Can not divide with tables!', cm.exception.args[0])

        # divide with 4D table
        d = BaseArray((3,3,3,3))

        with self.assertRaises(Exception) as cm:
            c = d / 2
        self.assertEqual('Only 1D,2D and 3D tables supported!', cm.exception.args[0])

    def test_log(self):
        # 1D table
        a = BaseArray((4,))
        a[1] = 1
        a[2] = 2
        a[3] = 3
        a[4] = 3
        a.logl()

        self.assertAlmostEqual(a[1], log(1, exp(1)), 6)
        self.assertAlmostEqual(a[2], log(2, exp(1)), 6)
        self.assertAlmostEqual(a[3], log(3, exp(1)), 6)
        self.assertAlmostEqual(a[4], log(3, exp(1)), 6)

        # 2D table
        a = BaseArray((4,4))
        a[1, 1] = 1
        a[2, 3] = 2
        a[3, 2] = 3
        a[4, 4] = 3
        a.logl()

        self.assertAlmostEqual(a[1, 1], log(1, exp(1)), 6)
        self.assertAlmostEqual(a[1, 2], 0)
        self.assertAlmostEqual(a[2, 3], log(2, exp(1)), 6)
        self.assertAlmostEqual(a[3, 2], log(3, exp(1)), 6)
        self.assertAlmostEqual(a[4, 4], log(3, exp(1)), 6)

        # 6D table
        a = BaseArray((3,3,3,4,4,4))
        a.logl()

        self.assertEqual(a[1, 1, 1, 1, 1, 1], 0)
        self.assertEqual(a[1, 1, 2, 1, 2, 1], 0)
        # and so on...

        # check invalid usage for negative numbers
        a = BaseArray((4,))
        a[1] = -4
        with self.assertRaises(Exception) as cm:
            a.logl()
        self.assertEqual('Log() does not support negative values!', cm.exception.args[0])

    def test_exp(self):
        # 1D table
        a = BaseArray((4,))
        a[1] = 0
        a[2] = 2
        a[3] = -3
        a[4] = 3
        a.expl()

        self.assertAlmostEqual(a[1], exp(0), 6)
        self.assertAlmostEqual(a[2], exp(2), 6)
        self.assertAlmostEqual(a[3], exp(-3), 6)
        self.assertAlmostEqual(a[4], exp(3), 6)

        # 2D table
        a = BaseArray((4, 4))
        a[1, 1] = 1
        a[2, 3] = 2
        a[3, 2] = 3
        a[4, 4] = 3
        a.expl()

        self.assertAlmostEqual(a[1, 1], exp(1), 6)
        self.assertAlmostEqual(a[1, 2], exp(0), 6)
        self.assertAlmostEqual(a[2, 3], exp(2), 6)
        self.assertAlmostEqual(a[3, 2], exp(3), 6)
        self.assertAlmostEqual(a[4, 4], exp(3), 6)

        # 6D table
        a = BaseArray((3, 3, 3, 4, 4, 4))
        a.expl()

        self.assertEqual(a[1, 1, 1, 1, 1, 1], exp(0))
        self.assertEqual(a[1, 1, 2, 1, 2, 1], exp(0))
        # and so on...

    def test_log_exp_inversion(self):
        a = BaseArray((7,))
        a[1] = 2
        a[2] = 100
        a[3] = 200
        a[4] = 300
        a[5] = 6.546
        a[6] = 1
        a[7] = 0
        a.logl()
        a.expl()

        self.assertAlmostEqual(a[1], 2, 4)
        self.assertAlmostEqual(a[2], 100, 4)
        self.assertAlmostEqual(a[3], 200, 4)
        self.assertAlmostEqual(a[4], 300, 4)
        self.assertAlmostEqual(a[5], 6.546, 4)
        self.assertAlmostEqual(a[6], 1, 4)

        self.assertNotAlmostEqual(a[7], 0, 4) # this will fail because LOG can't convert 0

    def test_multiplication_numbers(self):
        a = BaseArray((4,), int)
        a[1] = 0
        a[2] = -2
        a[3] = 2
        a[4] = 4

        self.assertEqual(a.dtype, int)

        c = a * 2
        self.assertEqual(c[1], a[1] * 2)
        self.assertEqual(c[2], a[2] * 2)
        self.assertEqual(c[3], a[3] * 2)
        self.assertEqual(c[4], a[4] * 2)
        self.assertEqual(c.dtype, int)

        c = 2 * a
        self.assertEqual(c[1], a[1] * 2)
        self.assertEqual(c[2], a[2] * 2)
        self.assertEqual(c[3], a[3] * 2)
        self.assertEqual(c[4], a[4] * 2)
        self.assertEqual(c.dtype, int)

        c = a * 2.5
        self.assertEqual(c[1], a[1] * 2.5)
        self.assertEqual(c[2], a[2] * 2.5)
        self.assertEqual(c[3], a[3] * 2.5)
        self.assertEqual(c[4], a[4] * 2.5)
        self.assertEqual(c.dtype, float)

        # negative numbers
        c = a * -2.5
        self.assertEqual(c[1], a[1] * -2.5)
        self.assertEqual(c[2], a[2] * -2.5)
        self.assertEqual(c[3], a[3] * -2.5)
        self.assertEqual(c[4], a[4] * -2.5)
        self.assertEqual(c.dtype, float)

    def test_multiplication_tables(self):
        a = BaseArray((3,3), int)
        a[1, 1] = 1
        a[1, 2] = -4
        a[1, 3] = 5
        a[2, 1] = 11
        a[2, 2] = 5
        a[2, 3] = 0
        a[3, 1] = 6
        a[3, 2] = -12
        a[3, 3] = 0
        self.assertEqual(a.dtype, int)

        b = BaseArray((3, 3), float)
        b[1, 1] = 5
        b[1, 2] = -14
        b[1, 3] = 2.43
        b[2, 1] = 0.32
        b[2, 2] = 0
        b[2, 3] = -6.12
        b[3, 1] = 7.54
        b[3, 2] = 1
        b[3, 3] = 1
        self.assertEqual(b.dtype, float)

        i = BaseArray((3, 3), int)  # identity
        i[1, 1] = 1
        i[2, 2] = 1
        i[3, 3] = 1
        self.assertEqual(i.dtype, int)

        # test multiplication with identity ->  A * I = A
        c = a * i
        for x in range(3):
            for y in range(3):
                self.assertEqual(c[x + 1, y + 1], a[x + 1, y + 1])

        self.assertEqual(c.dtype, int) # data type should persist as INT

        # test multiplication with itself
        c = a * a
        self.assertEqual(c[1, 1], -13)
        self.assertEqual(c[1, 2], -84)
        self.assertEqual(c[1, 3], 5)
        self.assertEqual(c[2, 1], 66)
        self.assertEqual(c[2, 2], -19)
        self.assertEqual(c[2, 3], 55)
        self.assertEqual(c[3, 1], -126)
        self.assertEqual(c[3, 2], -84)
        self.assertEqual(c[3, 3], 30)
        self.assertEqual(c.dtype, int)  # data type should persist as INT

        # also test pow() while Im at it
        d = a.pow(2)
        for x in range(3):
            for y in range(3):
                self.assertEqual(c[x + 1, y + 1], d[x + 1, y + 1])

        # test multiplication with another FLOAT table
        c = a * b
        self.assertAlmostEqual(c[1, 1], 41.42, 5)
        self.assertAlmostEqual(c[1, 2], -9, 5)
        self.assertAlmostEqual(c[1, 3], 31.91, 5)
        self.assertAlmostEqual(c[2, 1], 56.6, 5)
        self.assertAlmostEqual(c[2, 2], -154, 5)
        self.assertAlmostEqual(c[2, 3], -3.87, 5)
        self.assertAlmostEqual(c[3, 1], 26.16, 5)
        self.assertAlmostEqual(c[3, 2], -84, 5)
        self.assertAlmostEqual(c[3, 3], 88.02, 5)
        self.assertEqual(c.dtype, float)  # data type should change to FLOAT

        c = b * a  # swap it around
        self.assertAlmostEqual(c[1, 1], -134.42, 5)
        self.assertAlmostEqual(c[1, 2], -119.16, 5)
        self.assertAlmostEqual(c[1, 3], 25, 5)
        self.assertAlmostEqual(c[2, 1], -36.4, 5)
        self.assertAlmostEqual(c[2, 2], 72.16, 5)
        self.assertAlmostEqual(c[2, 3], 1.6, 5)
        self.assertAlmostEqual(c[3, 1], 24.54, 5)
        self.assertAlmostEqual(c[3, 2], -37.16, 5)
        self.assertAlmostEqual(c[3, 3], 37.7, 5)
        self.assertEqual(c.dtype, float)  # data type should change to FLOAT

        # test it using GENERAL formula
        c = a * b
        self.assertAlmostEqual(c[1, 1], a[1,1]*b[1,1] + a[1,2]*b[2,1] + a[1,3]*b[3,1], 5)
        self.assertAlmostEqual(c[1, 2], a[1,1]*b[1,2] + a[1,2]*b[2,2] + a[1,3]*b[3,2], 5)
        self.assertAlmostEqual(c[1, 3], a[1,1]*b[1,3] + a[1,2]*b[2,3] + a[1,3]*b[3,3], 5)
        self.assertAlmostEqual(c[2, 1], a[2,1]*b[1,1] + a[2,2]*b[2,1] + a[2,3]*b[3,1], 5)
        self.assertAlmostEqual(c[2, 2], a[2,1]*b[1,2] + a[2,2]*b[2,2] + a[2,3]*b[3,2], 5)
        self.assertAlmostEqual(c[2, 3], a[2,1]*b[1,3] + a[2,2]*b[2,3] + a[2,3]*b[3,3], 5)
        self.assertAlmostEqual(c[3, 1], a[3,1]*b[1,1] + a[3,2]*b[2,1] + a[3,3]*b[3,1], 5)
        self.assertAlmostEqual(c[3, 2], a[3,1]*b[1,2] + a[3,2]*b[2,2] + a[3,3]*b[3,2], 5)
        self.assertAlmostEqual(c[3, 3], a[3,1]*b[1,3] + a[3,2]*b[2,3] + a[3,3]*b[3,3], 5)

    def test_multiplication_invalid(self):
        a = BaseArray((3, 3), int)
        a[1, 1] = 1
        a[1, 2] = -4
        a[1, 3] = 5
        a[2, 1] = 11
        a[2, 2] = 5
        a[2, 3] = 0
        a[3, 1] = 6
        a[3, 2] = -12
        a[3, 3] = 0

        b = BaseArray((4, 3), float)
        b[1, 1] = 5
        b[1, 2] = -14
        b[1, 3] = 2.43
        b[2, 1] = 0.32
        b[2, 2] = 0
        b[2, 3] = -6.12
        b[3, 1] = 7.54
        b[3, 2] = 1
        b[3, 3] = 1

        with self.assertRaises(Exception) as cm:
            c = a * b
        self.assertEqual('Number of columns on left must match number of rows on right table!', cm.exception.args[0])

        # 3D tables...
        b = BaseArray((3,3,3))
        with self.assertRaises(Exception) as cm:
            c = a * b
        self.assertEqual('Only 2D tables supported for table multiplication!', cm.exception.args[0])

    def test_izpis(self):
        # 1D
        a = BaseArray((4,))
        a[2] = 2
        a[3] = -20

        old_stdout = sys.stdout
        sys.stdout = mystdout = io.StringIO()
        
        a.izpis() 
        sys.stdout = old_stdout
        self.assertMultiLineEqual(mystdout.getvalue(), '0.0 2.0 -20.0 0.0 \n')

        # 2D
        a = BaseArray((3,3))
        a[1, 1] = -5
        a[1, 2] = 1
        a[1, 3] = 1
        a[2, 1] = 0
        a[2, 2] = 3
        a[2, 3] = 4
        a[3, 1] = 233
        a[3, 2] = 43
        a[3, 3] = 123

        old_stdout = sys.stdout
        sys.stdout = mystdout = io.StringIO()
        a.izpis()
        sys.stdout = old_stdout
        self.assertMultiLineEqual(mystdout.getvalue(), '  -5.0   1.0   1.0\n   0.0   3.0   4.0\n 233.0  43.0 123.0\n')


        # test unsupported dimension
        a = BaseArray((3,3,3,3))
        with self.assertRaises(Exception) as cm:
            a.izpis()
        self.assertEqual('Only 1D,2D and 3D tables supported!', cm.exception.args[0])